<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Multifix
 */

?>

	</div><!-- #content -->
	<!-- Scroll Top Button -->
	<a href="#top"></a>

	<footer id="colophon" class="site-footer">
        <div class="copyright">
            <div class="container">
                <div class="pull-left">
                    <p>© 2017. MULTIFIX Maintenence Services</p>
                </div>
                <div class="pull-right">
                    <p>Designed &amp; Developed by <a href="http://www.jadwa.ae/" target="_blank">JADWA</a></p>
                </div>
                <div class="clearfix"></div>
            </div>
		</div><!-- .site-info -->
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
