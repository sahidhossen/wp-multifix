<?php
/**
 * Created by PhpStorm.
 * User: sahidhossen.
 * Date: 4/9/18
 * Time: 10:05 PM
 */

// framework options filter example
function extra_cs_framework_options( $options ) {

    $options      = array(); // remove old options

    $options[]    = array(
        'name'      => 'header_section',
        'title'     => 'Header Section',
        'icon'      => 'fa fa-heart',
        'fields'    => array(
            array(
                'id'            => 'site_logo',
                'type'          => 'upload',
                'title'         => 'Upload Logo',
                'settings'      => array(
                    'upload_type'  => 'image',
                    'button_title' => 'Upload',
                    'frame_title'  => 'Select an image',
                    'insert_title' => 'Use this image',
                ),
            ),
            array(
                'id'            => 'zayed_logo',
                'type'          => 'upload',
                'title'         => 'Upload President Logo',
                'settings'      => array(
                    'upload_type'  => 'image',
                    'button_title' => 'Upload',
                    'frame_title'  => 'Select an image',
                    'insert_title' => 'Use this image',
                ),
            ),
            array(
                'id'              => 'social_icons',
                'type'            => 'group',
                'title'           => 'Social Links',
                'button_title'    => 'Add Social Links',
                'accordion_title' => 'Social Link',
                'fields'          => array(
                    array(
                        'id'      => 'social_icon',
                        'type'    => 'icon',
                        'title'   => 'Icon Field',
                        'default' => 'fa fa-heart',
                    ),
                    array(
                        'id'    => 'social_link',
                        'type'  => 'text',
                        'title' => 'Social Link',
                    ),
                    array(
                        'id'    => 'social_toggle',
                        'type'  => 'switcher',
                        'title' => 'Enable/Disabled',
                    ),
                ),
            ),

            array(
                'id'      => 'quotes_now_text', // another unique id
                'type'    => 'text',
                'title'   => 'Quotes Now Text',
                'default' => 'Quotes Now',
                'desc'    => 'Please set empty for hide quotes link',
            ),
            array(
                'id'      => 'quotes_now_link', // another unique id
                'type'    => 'text',
                'title'   => 'Quotes Now Link',
                'default' => '#',
            ),

            array(
                'id'      => 'quotes_now_arabic_text', // another unique id
                'type'    => 'text',
                'title'   => 'Quotes Now Arabic Text',
                'default' => 'طلب عرض أسعار',
                'desc'    => 'Please set empty for hide quotes link',
            ),
            array(
                'id'      => 'quotes_now_arabic_link', // another unique id
                'type'    => 'text',
                'title'   => 'Quotes Now Arabic Link',
                'default' => '#',
            ),
        )
    );

    
    return $options;
    
}
add_filter( 'cs_framework_options', 'extra_cs_framework_options' );
