<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Multifix
 */
?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">

	<?php wp_head(); ?>
	<script type="text/javascript">
		var templateUrl = '<?= get_bloginfo("template_url"); ?>';
	</script>
</head>

<body <?php body_class(); ?>>
<div id="page" class="site">
	<div id="preloader"></div>
	<!--header -->
	<header class="top-header">
	    <div class="container menu-container">
	      <div class="pull-left">
              <?php $header_logo = cs_get_option('site_logo'); ?>
              <div class="flex-box align-item-center logo-section">
                  <a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="Home">
                      <?php if( $header_logo == '' ) : ?>
                          <img src="<?php echo get_template_directory_uri() ?>/imgs/logo.png" class="logo" title="<?php bloginfo( 'name' ); ?>"  alt="<?php bloginfo( 'name' ); ?>"/>
                      <?php else: ?>
                          <img src="<?php echo $header_logo; ?>" class="logo" title="<?php bloginfo( 'name' ); ?>"  alt="<?php bloginfo( 'name' ); ?>"/>
                      <?php endif; ?>
                  </a>
                  <?php $president_logo = cs_get_option('zayed_logo'); ?>
                  <?php if($president_logo != '' ) : ?>
                  <div class="president-box">
                      <img src="<?= $president_logo; ?>" alt="President">
                      <p class="text-center"> YEAR OF </p>
                      <h3 class="title text-center"> ZAYED </h3>
                  </div>
                  <?php endif; ?>
              </div>
          </div>
	      <div class="pull-right">
	        <div class="top-menu">
                <?= wp_nav_menu( array( 'theme_location' => 'top_header_menu', 'container_class'=>'' ) ); ?>
	        </div>
	        <div class="clearfix"></div>
	        <div class="top-search-div">
	          <div class="top-social">
                  <?php
                   $social_icons = cs_get_option('social_icons');
                  ?>
                <?php if( count( $social_icons) > 0 ) : ?>
	            <ul>
                    <?php foreach( $social_icons as $social_icon ): ?>
                        <?php if( $social_icon['social_toggle'] ): ?>
                        <li>
                          <div class="hvr-sweep-to-right">
                              <a href="<?= $social_icon['social_link']; ?>" title="" target="_blank"><i class="<?= $social_icon['social_icon'] ?>"></i></a>
                          </div>
                        </li>
                            <?php endif; ?>
                    <?php endforeach; ?>
	          </ul>
              <?php endif; ?>
	          </div>
                  <!--
	          <div class="top-search top-s">
	            <input type="text" placeholder="Search" />
	            <input type="submit" class="search" value=""/>
	          </div>
                  -->
	        </div>
	      </div>
	    </div>
	    <div class="clearfix"></div>
	  </div>
	<!--\\ header -->
	<!-- Navigation -->
    <?php
    $quotes_text = cs_get_option('quotes_now_text');
    $quotes_link = cs_get_option('quotes_now_link');
    $quotes_arabic_text = cs_get_option('quotes_now_arabic_text');
    $quotes_arabic_link = cs_get_option('quotes_now_arabic_link');
    ?>
	<nav class="navbar navbar-default navbar-custom">
	  <div class="parallelogram-container">
	    <div class="parallelogram"></div>
	  </div>
	  <div class="container">
	    <div class="container-fluid">
	      <!-- Brand and toggle get grouped for better mobile display -->
	      <div class="navbar-header page-scroll">
	      <div class="top-search-div search-div search-div-2">
                   <!--
	            <div class="top-search top-search-2">
	              <input type="text" placeholder="Search" />
	              <input type="submit" class="search" value=""/>
	            </div>
                   -->
              <?php if( 'ar' == ICL_LANGUAGE_CODE ): ?>
              		<?php if($quotes_arabic_text != ''): ?><div class="pull-left quote-now quote-now-mobile"> <a href="<?= $quotes_arabic_link ?>"><?= $quotes_arabic_text; ?> <i class="fa fa-long-arrow-right" aria-hidden="true"></i> </a> </div> <?php endif; ?>
				<?php else: ?>
					 <?php if($quotes_text != ''): ?><div class="pull-left quote-now quote-now-mobile"> <a href="<?= $quotes_link ?>"><?= $quotes_text; ?> <i class="fa fa-long-arrow-right" aria-hidden="true"></i> </a> </div> <?php endif; ?>
				<?php endif; ?>
	          </div>
	        <div class="pull-left fixed-menu-logo-2"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="Home"><img src="<?php echo get_template_directory_uri() ?>/imgs/responsive-logo.png" alt="Logo"/></a> </div>
	        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1"> <span class="sr-only">Toggle navigation</span> <i class="fa fa-bars"></i></button>
	      </div>

	      <!-- Collect the nav links, forms, and other content for toggling -->
	      <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
	        <div class="pull-left fixed-menu-logo"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="Home"><img src="<?php echo get_template_directory_uri() ?>/imgs/responsive-logo.png" alt="Logo"/></a> </div>

					<?php
						wp_nav_menu( array(
					    'theme_location'	=> 'menu-1',
					    'depth'				=> 2, // 1 = with dropdowns, 0 = no dropdowns.
                        'container'			=> '',
                        'container_class'	=> '',
                        'container_id'		=> '',
                        'menu_class'		=> 'nav navbar-nav navbar-left',
					    'fallback_cb'		=> 'wp_bootstrap_navwalker::fallback',
					    'walker'			=> new wp_bootstrap_navwalker()
						) );
					?>

	        <?php if( 'ar' == ICL_LANGUAGE_CODE ): ?>
	        	<?php if($quotes_arabic_text != ''): ?><div class="pull-right quote-now"> <a href="<?= $quotes_arabic_link ?>"><?= $quotes_arabic_text; ?> <i class="fa fa-long-arrow-right" aria-hidden="true"></i> </a> </div> <?php endif; ?>
			<?php else: ?> 
				<?php if($quotes_text != ''): ?><div class="pull-right quote-now"> <a href="<?= $quotes_link ?>"><?= $quotes_text; ?> <i class="fa fa-long-arrow-right" aria-hidden="true"></i> </a> </div> <?php endif; ?>
			<?php endif; ?>
	        <div class="clearfix"></div>
	      </div>
	      <!-- /.navbar-collapse -->
	    </div>
	  </div>
	</nav>
	<!--\\ Navigation -->

	<div id="content" class="site-content">
