// Preloader
// makes sure the whole site is loaded
jQuery(window).on('load', function() {
"use strict";
    // will first fade out the loading animation
    jQuery("#status").fadeOut();
    // will fade out the whole DIV that covers the website.
jQuery("#preloader").delay(1000).fadeOut("slow");
  $("#preloader").fadeOut(1000, function() {
      $('body').removeClass('loading');
  });

})

// Scroll to top
function scrollToTop() {
"use strict";
	if ($('.scroll-top').length) {
        //Check to see if the window is top if not then display button
        $(window).on('scroll', function() {
            if ($(this).scrollTop() > 200) {
                $('.scroll-top').fadeIn();
            } else {
                $('.scroll-top').fadeOut();
            }
        });
        //Click event to scroll to top
        $('.scroll-top').on('click', function() {
            $('html, body').animate({
                scrollTop: 0
            }, 1500);
            return false;
        });
    }
}

// Top slider
// scroll to function
$("#scrollto").on('click', function() {
"use strict";
$('html, body').animate({
        scrollTop: $("#main-content").offset().top
    }, 900);
});

$("#subscribe").on('click', function() {
"use strict";
$('html, body').animate({
        scrollTop: $("#subscribe-main").offset().top
    }, 900);
});

// vertical tabs
var tabsFn = (function() {
"use strict";
function init() {
        // setHeight();
    }
    function setHeight() {
        var $tabPane = $('.tab-pane'),
            tabsHeight = $('.nav-tabs').height();

        $tabPane.css({
            height: tabsHeight
        });
    }

    $(init);
})();

// animation
"use strict";
			var wow = new WOW ({
				boxClass:     'wow',       // animated element css class (default is wow)
				animateClass: 'animated',  // animation css class (default is animated)
				offset:       120,         // distance to the element when triggering the animation (default is 0)
				mobile:       false,       // trigger animations on mobile devices (default is true)
				live:         true         // act on asynchronously loaded content (default is true)
			  }
			);
wow.init();
